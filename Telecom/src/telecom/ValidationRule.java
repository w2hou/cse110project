/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenhaoho
 */
public class ValidationRule {
    // Patterns for input validation
    private static final String USERNAME_PATTERN = "^[a-z0-9._-]{2,25}$";
    private static final String PASSWD_PATTERN = 
                "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!\\.]).{8,40})";
    private static final String EMAIL_PATTERN = 
                "^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$";
    // Files for storing data
    
    
    /**
     * Check if username meets the following requirements:
     *  - Between 2 and 25 characters long.
     *  - Contains characters, numbers and the ., -, _ symbols.
     * @param uName
     * @return true if username meets the requirements
     */
    public static boolean validateUsernameFormat(String uName){
        return uName.matches(USERNAME_PATTERN);
    }
    
    
    /**
     * Check if the password meets the following requirements:
     *  - Be between 8 and 40 characters long
     *  - Contain at least one digit.
     *  - Contain at least one lower case character.
     *  - Contain at least one upper case character.
     *  - Contain at least on special character from [ @ # $ % ! . ].
     * @param passwd Password to be validated
     * @return true if password meets the requirements
     */
    public static boolean validatePasswordFormat(String passwd) {   
        return passwd.matches(PASSWD_PATTERN);
    }
    
    
    /**
     * Check if the email enter follow the common format for email address
     */
    public static boolean validateEmailFormat(String email) {
        return email.matches(EMAIL_PATTERN);
    }
}

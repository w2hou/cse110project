/*
 * In charge of progression of user interface.
 */
package telecom;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Wen-Hao Hou
 */
public class Telecom extends Application {
    private Stage stage; 
    
  
    /**
     * Starts the program
     * @param primaryStage The primary stage that contains all the scenes
     */
    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        
        Welcome.welcomeScene(stage);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

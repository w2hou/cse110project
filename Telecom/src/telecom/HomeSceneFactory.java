/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public final class HomeSceneFactory {
    protected static HomeSceneInterface createHomeScene(User user)
    {
        HomeSceneInterface hsi;
        if (user.getUType() == User.UserType.RETAIL)
            hsi = new HomeSceneRetail();
        else if (user.getUType() == User.UserType.COMMERCIAL)
            hsi = new HomeSceneCommercial();
        else if (user.getUType() == User.UserType.CUSTREP)
            hsi = new HomeSceneCustRep();
        else
            hsi = new HomeSceneMarkRep();
            
        return hsi;
    }
}

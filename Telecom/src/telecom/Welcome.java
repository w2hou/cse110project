/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public final class Welcome {
    public static void welcomeScene(Stage stage) {
        // Button to go to login page
        Button btnLogIn = new Button("Log In");
        btnLogIn.setMaxWidth(Double.MAX_VALUE);
        btnLogIn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // if button is clicked, go to log in scene
                Login.loginScene(stage);
            }
        });
        
        // Button to go to register page
        Button btnRegister = new Button("Create Account");
        btnRegister.setMaxWidth(Double.MAX_VALUE);
        btnRegister.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // if button is click, go to register scene
                Register.registerScene(stage); 
            }
        });
        
        // Put the two buttons in virtical box
        VBox vbButtons = new VBox();
        vbButtons.setAlignment(Pos.CENTER);
        vbButtons.setSpacing(10);
        vbButtons.setPadding(new Insets(0, 20, 10, 20));
        vbButtons.getChildren().addAll(btnLogIn, btnRegister);
        
        
        // Create root scene
        Scene scene = new Scene(vbButtons, 300, 275);
        
        stage.setTitle("Welcome");
        stage.setScene(scene);
        stage.setMinHeight(110);
        stage.setMinWidth(160);
        stage.show();
    }
}

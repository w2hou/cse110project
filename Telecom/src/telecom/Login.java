/*
 * Use AuthenticationRule to check username and password. If passed authentication,
 * then 
 */
package telecom;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public final class Login {
    
    /**
     * The scene for the Login page.
     */
    public static void loginScene(Stage stage) {
        // Create GridPane for the scene
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        // Create nodes and add them to GridPane
        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label uName = new Label("User Name:");
        grid.add(uName, 0, 1);
        
        TextField tfUName = new TextField();
        grid.add(tfUName, 1, 1);
        
        Label passwd = new Label("Password:");
        grid.add(passwd, 0, 2);
        
        PasswordField pfPasswd = new PasswordField();
        grid.add(pfPasswd, 1, 2);
        
        // Create a HBox for the Log in button
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        Button btn = new Button("Log in");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                // todo
            }
        });
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 3);
        
        // Create actiontarget to display error message
        final Text actiontarget = new Text();
        grid.add(actiontarget, 0, 5, 2, 1);
        
        // Create scene
        Scene loginScene = new Scene(grid, 400, 300);
        
        // Set stage dimention and update
        stage.setTitle("Log in");
        stage.setScene(loginScene);
        stage.setMinHeight(300);
        stage.setMinWidth(300);
        stage.show();
        
        // Handle event when Log in button is clicked
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String uName, password;
                uName = tfUName.getText();
                password = pfPasswd.getText();
                // if button is clicked, run the loginUser method
                // in the Login class
                Login.loginUser(stage, grid, actiontarget, uName, password);
                
            }
        });
    }
    
    
    public static void loginUser(Stage stage, GridPane grid, Text actiontarget,
                                 String uName, String password) {
        Boolean passAuthentication  = true;
        String errorMessage = "Log in failed.\n";
        
        // Check if user name entered exists in data file
        if (! AuthenticationRule.userNameExist(uName)) {
            passAuthentication = false;
        }
        
        // Check if password matches password of said user
        if (! AuthenticationRule.passwdMatch(uName, password)) {
            passAuthentication = false;
        }
        
        // If pass all authentication requirement create user object and go to 
        // home scene
        if (passAuthentication) {
            // Create user object
            String[] userInfos = DataFile.getUserInfos(uName);
            Enum uType = User.UserType.valueOf(userInfos[4]);
            User user = UserFactory.makeUser(userInfos, uType);
            
            // Go to home scene
            HomeSceneInterface hsi = HomeSceneFactory.createHomeScene(user);
            hsi.startHomeScene(stage, user);
        }
        else {
            // Show error message in actiontarget
            actiontarget.setFill(Color.FIREBRICK);
            actiontarget.setWrappingWidth(grid.getWidth());
            actiontarget.setText(errorMessage);
            stage.setMinWidth(300);
            stage.setMinHeight(350);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public class HomeSceneRetail implements HomeSceneInterface {
    
    @Override
    public void startHomeScene(Stage stage, User user) {
        TabPane tp = new TabPane();
        BorderPane bp = new BorderPane();
        Group root = new Group();
        Scene scene = new Scene(root, 700, 550);
        
        // Create tabs
        Tab tabA = new Tab();
        tabA.setText("Account Info");
        tabA.setContent(HomeSceneComponents.userAccountInfo(user));
        tp.getTabs().add(tabA);
        
        Tab tabB = new Tab();
        tabB.setText("Services");
        tabB.setContent(HomeSceneComponents.existingService(user));
        tp.getTabs().add(tabB);
        
        Tab tabC = new Tab();
        tabC.setText("Packages");
        tabC.setContent(HomeSceneComponents.existingPackage(user));
        tp.getTabs().add(tabC);
        
        Tab tabD = new Tab();
        tabD.setText("Bill");
        tp.getTabs().add(tabD);
        
        
        // Set closing policy so that tab cannot be closed
        tp.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        
        bp.setTop(HomeSceneComponents.headerComponent(stage));
        bp.setCenter(tp);
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());
        
        root.getChildren().add(bp);
        stage.setTitle("Telecom");
        stage.setScene(scene);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.show();
    }
    
}

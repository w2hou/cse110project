/* Generte scene for creating user and uses ValidationRule's methods to check 
 * user input. If pass all validation then generate user account and write to 
 * account data file.
 */
package telecom;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public final class Register {  
    private static String fName;
    private static String lName;
    private static String email;
    private static String uName;
    private static String passwd;
    private static String passwdConf;
    private static Enum userType;
    
    
    /**
     * The scene for the Create User page. Contains the form
     * for registering and use the input to modify account file
     */
    public static void registerScene(Stage stage) {
        // Create GridPane for the form 
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        // Create nodes and add them to GridPane
        Text scenetitle = new Text("Create User");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label lbFName = new Label("First name:");
        grid.add(lbFName, 0, 1);
        
        TextField tfFName = new TextField();
        grid.add(tfFName, 1, 1);
        
        Label lbLName = new Label("Last name:");
        grid.add(lbLName, 0, 2);
        
        TextField tfLName = new TextField();
        grid.add(tfLName, 1, 2);
        
        Label lbEmail = new Label("Email:");
        grid.add(lbEmail, 0, 3);
        
        TextField tfEmail = new TextField();
        grid.add(tfEmail, 1, 3);
        
        Label lbUName = new Label("User Name:");
        grid.add(lbUName, 0, 6);
        
        TextField tfUName = new TextField();
        grid.add(tfUName, 1, 6);
        
        Label lbPasswd = new Label("Password:");
        grid.add(lbPasswd, 0, 7);
        
        PasswordField pfPasswd = new PasswordField();
        grid.add(pfPasswd, 1, 7);
        
        Label lbPasswdConf = new Label("Confirm Password:");
        grid.add(lbPasswdConf, 0, 8);
        
        PasswordField pfPasswdConf = new PasswordField();
        grid.add(pfPasswdConf, 1, 8);
        
        Label lbUType = new Label("User Type:");
        grid.add(lbUType, 0, 4);
        
        // Create a group of radio buttons to determine user type
        final ToggleGroup group = new ToggleGroup();
        RadioButton rb1 = new RadioButton("Retail");
        rb1.setToggleGroup(group);
        rb1.setSelected(true);
        grid.add(rb1, 1, 4);
        
        RadioButton rb2 = new RadioButton("Commercial");
        rb2.setToggleGroup(group);
        grid.add(rb2, 1, 5);
        
        // Create a HBox for the submit button
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        Button btn = new Button("Submit");
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 9);
        
        // Create actiontarget to display error message
        final Text actiontarget = new Text();
        grid.add(actiontarget, 0, 11, 2, 2);
        
        Scene regScene = new Scene(grid, 400, 400);
        
        // Set stage dimention and update
        stage.setTitle("Create User");
        stage.setScene(regScene);
        stage.setMinHeight(400);
        stage.setMinWidth(400);
        stage.setMaxHeight(600);
        stage.setMaxWidth(550);
        stage.show();
        
        // Handle event when submit button is clicked
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fName = tfFName.getText();
                lName = tfLName.getText();
                email = tfEmail.getText();
                uName = tfUName.getText();
                passwd = pfPasswd.getText();
                passwdConf = pfPasswdConf.getText();
                // if button is clicked, run the createUser method
                // in the Register class
                Register.createUser(stage, grid, actiontarget, rb1.isSelected());
                
            }
        });
    }
    
    
    /**
     * 
     * @param actiontarget For outputting error message
     * @param userInfos Array of strings of user info
     * @param retail 
     */
    private static void createUser(Stage stage, GridPane grid, Text actiontarget,
        Boolean retail) {
        String[] userInfos = {uName, fName, lName, email, passwd, passwdConf};
        Boolean passValidation = true;
        String errorMessage = "";

        if (retail)
        {
            userType = User.UserType.RETAIL;
        }
        else
        {
            userType = User.UserType.COMMERCIAL;
        }
        
        // Check if all fields are filled
        for (String userInf: userInfos) {
            if (userInf.isEmpty()) {
                errorMessage += "All information must be provided.\n";
                passValidation = false;
                break;
            }
        }
        // Check that there is no colon in the entries
        for (int i = 0; i < 4; i++) {
            String s = userInfos[i];
            if (s.contains(":")) {
                errorMessage += "Entry/entries should not contain colon.\n";
                passValidation = false;
                break;
            }
        }
        
        if (! ValidationRule.validateUsernameFormat(uName)) {
            errorMessage += "User name should be 2 to 25 characters long " +
                " and only contains characters, numbers, and the ., -, _ " + 
                "symbols.\n";
            passValidation = false;
        }
        
        if (AuthenticationRule.userNameExist(uName)) {
            errorMessage += "Username unavailable.\n";
            passValidation = false;
        }
        
        if (! ValidationRule.validateEmailFormat(email)) {
            errorMessage += "Email format incorrect.\n";
            passValidation = false;
        }
        
        if (! ValidationRule.validatePasswordFormat(passwd)) {
            errorMessage += "Password must be between 8 and 40 characters " + 
                " long and contains at least one upper case, one lower case, " +
                "and one special character [@ # $ % ! .]\n";
            passValidation = false;
        }
        
        if (! passwd.equals(passwdConf)) {
            errorMessage += "Passwords do not match.\n";
            passValidation = false;
        }
        
        // If entries pass all validation test, create the user in text file
        if (passValidation) {
            // Create user object and write information to data files
            User user = UserFactory.makeUser(userInfos, userType);
            DataFile.writeToAccount(user);
            DataFile.writeToPasswd(uName, passwd);
            
            // Go to home scene
            HomeSceneInterface hsi = HomeSceneFactory.createHomeScene(user);
            hsi.startHomeScene(stage, user);
        }
        else {
            // Show error message in actiontarget
            actiontarget.setFill(Color.FIREBRICK);
            actiontarget.setWrappingWidth(grid.getWidth());
            actiontarget.setText(errorMessage);
            stage.setMinWidth(450);
            stage.setMinHeight(550);
        }
    }
}

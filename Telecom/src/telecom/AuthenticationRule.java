/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wenhaoho
 */
public class AuthenticationRule {
    /**
     * Check if the username is already exist
     * @param username User's input username
     * @return true if username exist in the account.data file
     */
    public static boolean userNameExist(String username) {
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(DataFile.ACCOUNTFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (username.equals(field[0])) 
                    return true;
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    
    /**
     * 
     * @param username User name entered
     * @param password Password entered
     * @return true if the password entered matches the password corresponding to
     *               the user name
     */
    public static boolean passwdMatch(String username, String password) {
        String line;
        String encrypted = DataFile.encryptPasswd(password);
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(DataFile.PASSWDFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (username.equals(field[0]) && encrypted.equals(field[1])) {
                    return true;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
}

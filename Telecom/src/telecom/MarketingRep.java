/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

/**
 *
 * @author wenhaoho
 */
public class MarketingRep extends User {
    /**
     * Constructor for MarketingRep
     * @param uname Username of marketing rep
     * @param fname First name of marketing rep
     * @param lname Last name of marketing rep
     * @param uType User type
     * @param email Marketing rep's email
     */
    public MarketingRep(String uname, String fname, String lname, 
                        String email, Enum uType) {
        super(uname, fname, lname, email, uType);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

/**
 *
 * @author wenhaoho
 */
public class RetailCustomer extends User {
    
    /**
     * Constructor for RetailCustomer
     * @param uname Username of customer
     * @param fname First name of customer
     * @param lname Last name of customer
     * @param uType User type
     * @param email Customer's email
     */
    public RetailCustomer(String uname, String fname, String lname,
                             String email, Enum uType) {
        super(uname, fname, lname, email, uType);
    }
}

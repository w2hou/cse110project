/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

/**
 *
 * @author wenhaoho
 */
public class CustomerRep extends User {
    
    /**
     * Constructor for CustomerRep
     * @param uname Username of customer rep
     * @param fname First name of customer rep
     * @param lname Last name of customer rep
     * @param uType User type
     * @param email Customer rep's email
     */
    public CustomerRep(String uname, String fname, String lname, 
                        String email, Enum uType) {
        super(uname, fname, lname, email, uType);
    }
}

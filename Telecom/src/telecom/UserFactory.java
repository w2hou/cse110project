/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

/**
 *
 * @author wenhaoho
 */
public final class UserFactory {
    /**
     *
     * @param userInfos an array of String containing uName, fName, lName, email
     * @return User object
     */
    protected static User makeUser(String[] userInfos, Enum userType) {
        User user = null;
        
        if (userType == User.UserType.RETAIL) {
            user = new RetailCustomer(userInfos[0], userInfos[1], userInfos[2],
                                      userInfos[3], User.UserType.RETAIL);
        }
        else if (userType == User.UserType.COMMERCIAL) {
            user = new CommercialCustomer(userInfos[0], userInfos[1], userInfos[2],
                                         userInfos[3], User.UserType.COMMERCIAL);
        }
        else if(userType == User.UserType.CUSTREP) {
            user = new CustomerRep(userInfos[0], userInfos[1], userInfos[2],
                                   userInfos[3], User.UserType.CUSTREP);
        }
        else if (userType == User.UserType.MARKETREP) {
            user = new MarketingRep(userInfos[0], userInfos[1], userInfos[2],
                                    userInfos[3], User.UserType.MARKETREP);
        }
        
        return user;
    }
}

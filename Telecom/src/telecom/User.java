/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

/**
 *
 * @author wenhaoho
 */
public abstract class User {
    final private Enum userType;
    private String fName;
    private String lName;
    final private String uName;
    private String email;
    public static enum UserType {
        RETAIL, COMMERCIAL, MARKETREP, CUSTREP;
    }
    
    /**
     * Constructor for User
     * @param uname Username of user
     * @param fname First name of user
     * @param lname Last name of user
     * @param uType User type
     * @param email User's email
     */
    public User(String uname, String fname, String lname, 
                 String email, Enum uType) {
        userType = uType;
        this.fName = fname;
        this.lName = lname;
        this.uName = uname;
        this.email = email;
    }
    
    public String getUName() {
        return uName;
    }
    
    public String getFName() {
        return fName;
    }
    
    public String getLName() {
        return lName;
    }
    
    public Enum getUType() {
        return userType;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setFName(String fname) {
        fName = fname;
    }
    
    public void setLName(String lname) {
        lName = lname;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
}

/*
 * Contains methods for writing to and accessing the data files
 */
package telecom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author wenhaoho
 */
public final class DataFile {
    protected static final String ACCOUNTFILE = "data/account.data";
    protected static final String PASSWDFILE = "data/password.data";
    protected static final String SERVICEFILE = "data/service.data";
    protected static final String PACKAGEFILE = "data/package.data";
    
    
    /**
     * Write user information into the account file. Use for creating new user.
     * @param user user object to get user information from
     */
    protected static void writeToAccount(User user) {
        String accountData;
        accountData = user.getUName() + ":" + user.getUType() + ":"+ 
                user.getLName() + ":" + user.getFName() + ":" + 
                user.getEmail() + ":" + "0" + ":" + "0";
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(
                new FileWriter(ACCOUNTFILE, true)))) {
                pw.println(accountData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    /**
     * Write user name and encrypted password into password file
     * @param uName username to be written into password file
     * @param passwd encrypted password to be written into the password file
     */
    protected static void writeToPasswd(String uName, String passwd) {
        String passwdData;
        passwdData = uName + ":" + encryptPasswd(passwd);
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(
                new FileWriter(PASSWDFILE, true)))) {
                pw.println(passwdData);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    /**
     * Encrypt password using MD5 message digest algorithm
     * @param passwd password to be encrypted
     * @return encrypted password as String
     */
    protected static String encryptPasswd(String passwd) {
        String encryptedPasswd = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(passwd.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b: digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            encryptedPasswd += sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encryptedPasswd;
    }
    
    
    /**
     * Return a list of the username of all existing users in sorted order
     * @return a string array of usernames
     */
    protected static String[] getListOfUser() {
        List<String> arrayListOfUsers = new ArrayList<>();
        String[] listOfUsers = null;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(ACCOUNTFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                arrayListOfUsers.add(field[0]);
            }
            Collections.sort(arrayListOfUsers);
            listOfUsers = arrayListOfUsers.toArray(new String[arrayListOfUsers.size()]);
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfUsers;        
    }
    
    
    /**
     * Return a list of index of addable services in sorted order
     * @return an integer array of addable services
     */
    protected static Integer[] getListOfAddableServices() {
        List<Integer> alAddableServices = new ArrayList<>();
        Integer[] listOfAddableServices = null;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(SERVICEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (field[1].equals("true")) {
                    alAddableServices.add(Integer.parseInt(field[0]));
                }
            }
            Collections.sort(alAddableServices);
            listOfAddableServices = alAddableServices.toArray(
                        new Integer[alAddableServices.size()]);
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfAddableServices;         
    }
    
    
    /**
     * Return a list of index of all services in sorted order
     * @return an integer array of all services
     */
    protected static Integer[] getListOfAllServices() {
        List<Integer> alAllServices = new ArrayList<>();
        Integer[] listOfAllServices = null;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(SERVICEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                alAllServices.add(Integer.parseInt(field[0]));
            }
            Collections.sort(alAllServices);
            listOfAllServices = alAllServices.toArray(
                        new Integer[alAllServices.size()]);
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfAllServices;         
    }    
    
    
    /**
     * Return a list of index of addable packages in sorted order
     * @return an integer array of addable packages
     */
    protected static Integer[] getListOfAddablePackages() {
        List<Integer> alAddablePackages = new ArrayList<>();
        Integer[] listOfAddablePackages = null;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(PACKAGEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (field[1].equals("true")) {
                    alAddablePackages.add(Integer.parseInt(field[0]));
                }
            }
            Collections.sort(alAddablePackages);
            listOfAddablePackages = alAddablePackages.toArray(
                        new Integer[alAddablePackages.size()]);
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfAddablePackages;         
    }
    
    
    /**
     * Return a list of index of all packages in sorted order
     * @return an integer array of all packages
     */
    protected static Integer[] getListOfAllPackages() {
        List<Integer> alAllPackages = new ArrayList<>();
        Integer[] listOfAllPackages = null;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(PACKAGEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                alAllPackages.add(Integer.parseInt(field[0]));
            }
            Collections.sort(alAllPackages);
            listOfAllPackages = alAllPackages.toArray(
                        new Integer[alAllPackages.size()]);
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listOfAllPackages;         
    }    
    
    
    /**
     * Read the user information from the account file
     * @param username username of the user whose info is returned
     * @return an array of String containing uName, fName, lName, email, uType,
     *             a list of services and a list of packages
     */
    protected static String[] getUserInfos(String username) {
        String[] userInfos = new String[7];
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(ACCOUNTFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (username.equals(field[0])) {
                    userInfos[0] = field[0]; // uName
                    userInfos[1] = field[3]; // fName
                    userInfos[2] = field[2]; // lName
                    userInfos[3] = field[4]; // email
                    userInfos[4] = field[1]; // uType
                    userInfos[5] = field[5]; // services
                    userInfos[6] = field[6]; // packages
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return userInfos;
    }
    
    
    /**
     * Return the service information from the service file
     * @param serviceId the id of the service whose info is returned
     * @return an array of String containing service id, addable, service name,
     *          and service price
     */
    protected static String[] getServiceInfos(String serviceId) {
        String[] serviceInfos = new String[4];
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(SERVICEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (serviceId.equals(field[0])) {
                    serviceInfos[0] = field[0]; // Service id
                    serviceInfos[1] = field[1]; // Whether or not service is addable
                    serviceInfos[2] = field[2]; // Service name
                    serviceInfos[3] = field[3]; // Service price
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return serviceInfos;        
    }
    
    
    /**
     * Return the package information from the package file
     * @param packageId the id of the package whose info is returned
     * @return an array of String containing package id, addable, package name,
     *          package price, and a list of services in the package
     */
    protected static String[] getPackageInfos(String packageId) {
        String[] packageInfos = new String[5];
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(PACKAGEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (packageId.equals(field[0])) {
                    packageInfos[0] = field[0]; // Package id
                    packageInfos[1] = field[1]; // Whether or not service is addable
                    packageInfos[2] = field[2]; // Package name
                    packageInfos[3] = field[3]; // Package price
                    packageInfos[4] = field[4]; // List of services delimited by ","
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return packageInfos;        
    }    
    
    
    /**
     * Return the IDs of services that the user subscribes to
     * @param username user name of the user whose services is returned
     * @return a String array of service IDs that the user subscribes to
     */
    protected static int[] getUserServices(String username) {
        String[] services;
        String[] userInfos = getUserInfos(username);
        services = userInfos[5].split(",");
        int[] serviceIds = new int[services.length];
        for (int i = 0; i < services.length; i++) {
            String[] serviceIndex = services[i].split("\\(");
            serviceIds[i] = Integer.parseInt(serviceIndex[0]);
        }
        
        return serviceIds;
    }    
    
    
    /**
     * Return the IDs of packages that the user subscribes to
     * @param username user name of the user whose packages is returned
     * @return a String array of package IDs that the user subscribes to
     */
    protected static int[] getUserPackages(String username) {
        String[] packages;
        String[] userInfos = getUserInfos(username);
        packages = userInfos[6].split(",");
        int[] packageIds = new int[packages.length];
        for (int i = 0; i < packages.length; i++) {
            String[] packageIndex = packages[i].split("\\(");
            packageIds[i] = Integer.parseInt(packageIndex[0]);
        }
        
        return packageIds;
    }    
    
    
    /**
     * Return the name of the services that an user own
     * @param username user name of the user whose service names are return
     * @return an array of String containing the name of the services that
     *          the user own
     */
    protected static String[] getUserServiceNames(String username) {
        int[] serviceIds = getUserServices(username);
        String[] serviceNames = new String[serviceIds.length];
        for (int i = 0; i < serviceIds.length; i++) {
            String[] serviceInfos = getServiceInfos(
                        Integer.toString(serviceIds[i]));
            serviceNames[i] = serviceInfos[2];
        }
         
        return serviceNames;
    }  
    
    
    /**
     * Return the name of the packages that an user own
     * @param username user name of the user whose package names are return
     * @return an array of String containing the name of the packages that
     *          the user own
     */
    protected static String[] getUserPackageNames(String username) {
        int[] packageIds = getUserPackages(username);
        String[] packageNames = new String[packageIds.length];
        for (int i = 0; i < packageIds.length; i++) {
            String[] packageInfos = getPackageInfos(
                        Integer.toString(packageIds[i]));
            packageNames[i] = packageInfos[2];
        }
         
        return packageNames;
    }  
    
    
    /**
     * Given a list of service ids, returns the corresponding service names
     * @param serviceIds an Integer array of service ids
     * @return a String array of service name corresponding to the service ids
     */
    protected static String[] getServiceNames(Integer[] serviceIds) {
        String[] field;
        String[] serviceNames = new String[serviceIds.length];
        for (int i = 0; i < serviceIds.length; i++) {
            String[] serviceInfos = getServiceInfos(
                        Integer.toString(serviceIds[i]));
            serviceNames[i] = serviceInfos[2];
        }
         
        return serviceNames;      
    }    
    
    
    /**
     * Given a list of package ids, returns the corresponding package names
     * @param packageIds an Integer array of package ids
     * @return a String array of package name corresponding to the package ids
     */
    protected static String[] getPackageNames(Integer[] packageIds) {
        String[] field;
        String[] packageNames = new String[packageIds.length];
        for (int i = 0; i < packageIds.length; i++) {
            String[] packageInfos = getPackageInfos(
                        Integer.toString(packageIds[i]));
            packageNames[i] = packageInfos[2];
        }
         
        return packageNames;      
    }
    
    /**
     * Return the prices of the services that an user owns
     * @param username user name of the user whose service prices are returned
     * @return an array of String containing the prices of the services that
     *          the user owns
     */
    protected static String[] getUserServicePrices(String username) {
        int[] serviceIds = getUserServices(username);
        String[] servicePrices = new String[serviceIds.length];
        for (int i = 0; i < serviceIds.length; i++) {
            String[] serviceInfos = getServiceInfos(
                        Integer.toString(serviceIds[i]));
            servicePrices[i] = serviceInfos[3];
        }
          
        return servicePrices;
    }
    
    
    /**
     * Return the prices of the services that an user owns
     * @param username user name of the user whose service prices are returned
     * @return an array of String containing the prices of the services that
     *          the user owns
     */
    protected static String[] getUserPackagePrices(String username) {
        int[] packageIds = getUserPackages(username);
        String[] packagePrices = new String[packageIds.length];
        for (int i = 0; i < packageIds.length; i++) {
            String[] packageInfos = getPackageInfos(
                        Integer.toString(packageIds[i]));
            packagePrices[i] = packageInfos[3];
        }
          
        return packagePrices;
    }  
    
    
    /**
     * Given a service name, return the service id
     * @return int value of service id
     */
    protected static int getServiceId(String serviceName) {
        int serviceId = -1;
        String line;
        String[] field;
        try (BufferedReader br = new BufferedReader(
                new FileReader(SERVICEFILE))) {
            while ((line = br.readLine()) != null) {
                field = line.split(":");
                if (field[2].equals(serviceName)) {
                    serviceId = Integer.parseInt(field[0]);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ValidationRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return serviceId; 
    }
    
    
    /**
     * Add a service to the user's account
     * @param user the user whose account the service is added to
     * @param serviceName name of the service being added
     * @param actiontarget for error message output
     */
    protected static void addService(User user, String serviceName,
            Text actiontarget) {
        int serviceId = getServiceId(serviceName);
        int[] userServices = getUserServices(user.getUName());
        String addDate = new SimpleDateFormat("(yyyyMMdd)").format(new Date());
        
        // Check if user already have the service being added
        for (int userService : userServices) {
            if (userService == serviceId) {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Service already subscribed to.");
                return;
            }
        }
        
        // Updating user info in account file
        String line;
        String[] field;
        PrintWriter pw;
        try {
            pw = new PrintWriter(new BufferedWriter(
                    new FileWriter("data/account.tmp")));
        
            try (BufferedReader br = new BufferedReader(
                    new FileReader(ACCOUNTFILE))) {
                while ((line = br.readLine()) != null) {
                    field = line.split(":");
                    if (! field[0].equals(user.getUName())) {
                        pw.println(line); // copy lines to tmp file
                    }
                    else {
                        field[5] += "," + serviceId + addDate;     
                        String userInfo = "";
                        for (String str : field) {
                            userInfo += str;
                        }
                        pw.println(userInfo); // print updated user info
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File realName = new File(ACCOUNTFILE); 
        realName.delete(); // Delete original account file
        // Change updated account.tmp to account.data
        new File("data/account.tmp").renameTo(realName); 
    }
    
    
    /**
     * Add a package to the user's account
     * @param user the user whose account the package is added to
     * @param packageName name of the package being added
     * @param actiontarget for error message output
     */
    protected static void addPackage(User user, String packageName,
            Text actiontarget) {
        
    }
    
    
    /**
     * Remove a service from the user's account
     * @param user the user whose account the service is removed from
     * @param serviceName name of the service being removed
     * @param actiontarget for error message output
     */
    protected static void removeService(User user, String serviceName, 
            Text actiontarget) {
        // to do
    }
    
    
    /**
     * Remove a package from the user's account
     * @param user the user whose account the package is removed from
     * @param packageName name of the package being removed
     * @param actiontarget for error message output
     */
    protected static void removePackage(User user, String packageName,
            Text actiontarget) {
        
    }
    
    
}

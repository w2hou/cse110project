/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package telecom;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 *
 * @author wenhaoho
 */
public final class HomeSceneComponents {
    /**
     * Return a log out button to be used in scenes
     * @param stage 
     * @return a button for logging out
     */
    protected static Button logOutButton(Stage stage) {
        Button btnLogOut = new Button("Log out");
        btnLogOut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // if button is click, go to register scene
                Welcome.welcomeScene(stage);
            }
        });
        
        return btnLogOut;
    }
    
    
    /**
     * Return header component including Company name and log out button
     * @param stage 
     * @return header node to be added into home scene
     */
    protected static Node headerComponent(Stage stage) {
        HBox header = new HBox();
        
        Text companyName = new Text("Telecom");
        companyName.setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.ITALIC, 24));
        companyName.setTextAlignment(TextAlignment.LEFT);
        
        Button btnLogOut = logOutButton(stage);
        btnLogOut.setAlignment(Pos.TOP_RIGHT);
        
        header.getChildren().addAll(companyName, btnLogOut);
        header.setSpacing(500);
        header.setPadding(new Insets(10, 0, 10, 10));
        
        return header;
    }
    
    
    /**
     * Return the component that shows the user's account info
     * @param user the logged in user
     * @return a Node to be added into one of home scene's account info tab
     */
    protected static Node userAccountInfo(User user) {
        VBox vb= new VBox();
        vb.setAlignment(Pos.TOP_LEFT);
        vb.setPadding(new Insets(25, 25, 25, 25));
        vb.setSpacing(5);
        
        // Text object that shows user's first name, last name and user type
        Text txtUser = new Text(user.getFName() + " " + user.getLName());
        Text txtUsertype = new Text("Account type: " + user.getUType());
                
        // Label and ListView that shows the services that user subscribes to
        Label lbServices = new Label("Subscribed services:");
        ListView<String> lvServices = new ListView<>();
        ObservableList<String> olServices = 
            FXCollections.observableArrayList(DataFile.getUserServiceNames(user.getUName()));
        lvServices.setItems(olServices);
        lvServices.setOrientation(Orientation.VERTICAL);
        lvServices.setPrefHeight(250);
        lvServices.setPrefWidth(300);
        
        // Action target for removing service
        final Text atRemoveService = new Text();
        
        // Remove service button
        Button btnRemoveService = new Button("Remove Service");
        btnRemoveService.setOnAction((ActionEvent event) -> {
            // if button is click, call remove service function
            String selectedService = lvServices.getSelectionModel().getSelectedItem();
            DataFile.removeService(user, selectedService, atRemoveService);
        });
        
        // Label and ListView that shows the packages that user subscribes to
        Label lbPackages = new Label("Subscribed packages:");
        ListView<String> lvPackages = new ListView<>();
        ObservableList<String> olPackages = 
            FXCollections.observableArrayList(DataFile.getUserPackageNames(user.getUName()));
        lvPackages.setItems(olPackages);
        lvPackages.setOrientation(Orientation.VERTICAL);
        lvPackages.setPrefHeight(250);
        lvPackages.setPrefWidth(300);
        
        // Action target for removing package
        final Text atRemovePackage = new Text();
        
        // Remove package button
        Button btnRemovePackage = new Button("Remove Package");
        btnRemovePackage.setOnAction((ActionEvent event) -> {
            // if button is click, call remove package function
            String selectedPackage = lvPackages.getSelectionModel().getSelectedItem();    
            DataFile.removePackage(user, selectedPackage, atRemovePackage);
        });
        
        // Putting control components into vertical box
        vb.getChildren().addAll(txtUser, txtUsertype, lbServices, lvServices,
                btnRemoveService, atRemoveService, lbPackages, lvPackages,
                btnRemovePackage, atRemovePackage);
   
        return vb;
    }
    
    
    /**
     * Return the components that shows existing services
     * @param user the logged in user
     * @return a Node to be added into one of home scene's services tab
     */
    protected static Node existingService(User user) {
        VBox vb = new VBox();
        vb.setAlignment(Pos.TOP_LEFT);
        vb.setPadding(new Insets(25, 25, 25, 25));
        vb.setSpacing(5);
        
        // Label and ListView of existing addable services
        Label lbServices = new Label("Services");
        ListView<String> lvServices = new ListView<>();
        Integer[] serviceIds = DataFile.getListOfAddableServices();
        ObservableList<String> olServices = 
            FXCollections.observableArrayList(DataFile.getServiceNames(serviceIds));
        lvServices.setItems(olServices);
        lvServices.setOrientation(Orientation.VERTICAL);
        lvServices.setPrefHeight(300);
        lvServices.setPrefWidth(300);  
        
        // Action target for adding service
        final Text atAddService = new Text();
        
        // Add service button
        Button btnAddService = new Button("Add Service");
        btnAddService.setOnAction((ActionEvent event) -> {
            // if button is click, call add service function
            String selectedService = lvServices.getSelectionModel().getSelectedItem();  
            DataFile.addService(user, selectedService, atAddService);
        });
                
        // Put UI control components into vertical box
        vb.getChildren().addAll(lbServices, lvServices, btnAddService,
                atAddService);
        
        return vb;
    }
    
    
    /**
     * Return the components that shows existing packages
     * @param user the logged in user
     * @return a Node to be added into one of home scene's packages tab
     */
    protected static Node existingPackage(User user) {
        VBox vb = new VBox();
        vb.setAlignment(Pos.TOP_LEFT);
        vb.setPadding(new Insets(25, 25, 25, 25));
        vb.setSpacing(5);
        
        // Action target for adding package
        final Text atAddPackage = new Text();
        
        // Label and ListView of existing addable packages
        Label lbPackages = new Label("Packages");
        ListView<String> lvPackages = new ListView<>();
        Integer[] packageIds = DataFile.getListOfAddablePackages();
        ObservableList<String> olPackages = 
            FXCollections.observableArrayList(DataFile.getPackageNames(packageIds));
        lvPackages.setItems(olPackages);
        lvPackages.setOrientation(Orientation.VERTICAL);
        lvPackages.setPrefHeight(300);
        lvPackages.setPrefWidth(300);        
        
        // Add service button
        Button btnAddPackage = new Button("Add Package");
        btnAddPackage.setOnAction((ActionEvent event) -> {
            // if button is click, call add package function
            String selectedPackage = lvPackages.getSelectionModel().getSelectedItem();   
            DataFile.addPackage(user, selectedPackage, atAddPackage);
        });
                
        // Put UI control components into vertical box
        vb.getChildren().addAll(lbPackages, lvPackages, btnAddPackage,
                atAddPackage);
        
        return vb;
    }    
}
